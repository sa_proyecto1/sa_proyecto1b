var hash = require('object-hash');
//numero random para meter al hash
const  TARGET_HASH = hash(1560);
var validator = require('./validator');

const mongoose = require('mongoose');
var chalk = require('chalk');
const blockChainModel = mongoose.model("BlockChain");

class BlockChain{
    constructor(){
        //crear
        this.chain = [];
        //transaccion
        this.curr_transactions = [];
    }

    getLastBlock(callback){
        //get last block from db
        return blockChainModel.findOne({}, null, {sort: { _id: -1 }, limit: 1 }, (err,block)=>{
            if(err) return console.error("No se pudo encontrar último bloque");
            return callback(block);
        })
    }
    addNewBlock(prevHash){
        let block = {
            index: this.chain.length + 1,
            timestamp: Date.now(),
            transactions: this.curr_transactions,
            
            prevHash: prevHash

        };
        if(validator.proofOfWork() == TARGET_HASH){
            
            
            block.hash = hash(block);
            
            this.getLastBlock((lastBlock)=>{
                if(lastBlock){
                    block.prevHash = lastBlock.hash;
                }
                
                //agregarlo al instance
                

                let newBlock = new blockChainModel(block);
                newBlock.save((err)=>{
                    if(err) 
                        return console.log(chalk.red("no se pudo guardar el bloque en la bd", err.message));
                    console.log(chalk.green("bloque guardado en la bd"));
                });
                //insertarlo en la bd
                //mensaje de exito
            
                //put hash
                this
                    .chain
                    .push(block);
                this.curr_transactions = [];
                return block;
                // add to chain
                
            });
        }
    }

    addNewTransaction(id_usuario, id_eleccion, id_opcion){
        this.curr_transactions.push({id_usuario, id_eleccion, id_opcion});
        console.log("Insertado en blockchain",id_usuario," ", id_eleccion," ", id_opcion)
    }


    isEmpty(){
        return this.chain.length == 0;
    }

    lastBlock(){
        return this.chain.slice(-1)[0];
    }

}

module.exports = BlockChain;