const express = require('express');
const app = express();
const mysql = require('mysql');
const morgan = require('morgan');
const bodyParser = require('body-parser');
var cors = require('cors');

const myConnection = require('express-myconnection');

var jwt=require('jsonwebtoken');
const fs = require('fs');
const path = require('path');
require('./database');

const Resultados=require('../app/resultados');

let microservicio=new Resultados.Resultados();

let respuesta = {
    error: false,
    codigo: 200
   };

var publicKey = fs.readFileSync('app/public.key','utf8');

var verifyOptions = {
    algorithms: ["RS256"],
    maxAge: "60s"    
};

// request log
app.use(morgan('dev'));
app.use(cors());
// connection to BD
/*
app.use(myConnection(mysql, {
    host: 'db-mysql',
    user: 'root',
    password: 'root',
    port: 3306,
    database: 'sa_bd'
  }, 'single'));
*/
app.use(express.urlencoded({extended: true}));

app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(bodyParser.json());

app.listen(3005, ()=>{
    console.log('server on port 3005');
})

/*
const pool = mysql.createPool({
    connectionLimit: 10,    
    host: 'db-mysql',
    user: 'root',
    password: 'root',
    port: 3306,
    database: 'sa_bd'
 
}); 
 */


votantesRegistrados = () =>{
    return new Promise((resolve, reject)=>{
        pool.query(`select count(*) 
            from Ciudadano c left join detalle_eleccion de 
            on c.idCiudadano=de.idCiudadano and de.idEleccion= ?  `, ideleccion,  (error, elements)=>{
            if(error){
                return reject(error);
            }
            return resolve(elements);
        });
    });
};


// Verificar token para autorizar o denegar el acceso
function verifytoken(req,res,next){
    const bearerHeader=req.headers['authorization'];

    if(typeof bearerHeader !=='undefined'){
        const bearerToken=bearerHeader.split(" ")[1];
        req.jwt=bearerToken;
        next();
    }else{
        res.sendStatus(403) 
    }
}

// Consulta el total de votantes registrados
function obtenerTotalVotantesRegistrados(ideleccion){
    req.getConnection((err, conn) => {
        conn.query(`select count(*) 
            from detalle_eleccion de inner join eleccion e
            on de.idEleccion=e.idEleccion and e.idEleccion=? `, ideleccion, (err, rows) => {
            res.json({resultado:rows});
        });
    });
}



// Consultar resultados de votaciones
app.get('/api/:ideleccion', async function (req, res) {
    const  id  = req.params.ideleccion;
    res.json(resultados);
    
});

// Consultar resultados de votaciones
app.get('/partidos/:ideleccion', verifytoken, async function (req, res) {

    const  id  = req.params.ideleccion;
    //res.send("Hola");
    
    respuesta.error=true;

    jwt.verify(req.jwt,publicKey,verifyOptions,(err)=>{
        if(err){ 
            respuesta.codigo=403;
            res.send(respuesta);
        }else{
            if(microservicio.isIdValido(id)){
                //var decoded = jwt.decode(req.jwt, {complete: true});

                microservicio.llamarMicroserviciosVotacion(id,req.jwt,function(err,data){
                    if(err){
                        respuesta.codigo=404;
                        res.send(respuesta);
                    }else{
                        if(this.isValidJsonResult(res)){
                            res.json({resultado:data});
                        }else{
                            respuesta.codigo=data.status;
                            res.json({respuesta});
                        }
            
                    }
                });                
                /*
                req.getConnection((err, conn) => {
                    conn.query('select titulo, fecha_inicio, fecha_fin from Eleccion where idEleccion=? ', id, (err, rows) => {
                        res.json({resultado:rows});
                    });
                });
                */
            }else{
                respuesta.error=true;
                respuesta.codigo=404;
                res.send(respuesta);
            }
        }
    });
});