var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var _this = this;
var express = require('express');
var db = require('../db/Index');
var axios = require('axios');
var bodyParser = require('body-parser');
var router = express.Router();
var fs = require('fs');
/*
const path = require('path');
const jwt = require('jsonwebtoken');
*/
var logger = fs.createWriteStream('./log.txt', {
    flags: 'a' // 'a' means appending (old data will be preserved)
});
function addLog(newlog) {
    logger.write(newlog + "\n");
}
var http = require('http');
//TODO ESTO ES PARA DESENCRIPTAR TOKENS QUE SOLICITAN CONSUMIR TUS SERVICIOS DE TU MICROSERVICIO
//var publicKey = fs.readFileSync('./server/routes/public.key','utf8');
var verifyOptions = {
    algorithms: ["RS256"],
    maxAge: "60s"
};
router.get('/listaregistro', function (req, res, next) { return __awaiter(_this, void 0, void 0, function () {
    var results, e_1;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                return [4 /*yield*/, db.all()];
            case 1:
                results = _a.sent();
                res.json(results);
                return [3 /*break*/, 3];
            case 2:
                e_1 = _a.sent();
                console.log(e_1);
                res.sendStatus(500);
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); });
router.get('/getregistro', function (req, res, next) { return __awaiter(_this, void 0, void 0, function () {
    var results, e_2;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                return [4 /*yield*/, db.all()];
            case 1:
                results = _a.sent();
                res.json(results);
                return [3 /*break*/, 3];
            case 2:
                e_2 = _a.sent();
                console.log(e_2);
                res.sendStatus(500);
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); });
router.get('/getpais', function (req, res, next) { return __awaiter(_this, void 0, void 0, function () {
    var results, e_3;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                return [4 /*yield*/, db.allPais()];
            case 1:
                results = _a.sent();
                res.json(results);
                addLog("paises consultados" + "\n");
                return [3 /*break*/, 3];
            case 2:
                e_3 = _a.sent();
                console.log(e_3);
                res.sendStatus(500);
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); });
/*
// ruta index
router.get('/index', function (req, res) {
    console.log("Ruta index");
    res.json({
        mensaje: "Microservicio de Autenticacion"
    });
});

// endpoint para el login de usuarios
router.post('/login',function (req, res) {
    const user = req.body;
    req.getConnection((err, conn) => {
        conn.query("SELECT * FROM usuario WHERE correo = ?", [user.correo], (err, rows) => {
            if(err){
                console.log(err);
                res.json(err);
            }
            if(user.password == rows[0].password){
                addLog(" >> Login satisfactorio para usuario ");
                res.json({
                    mensaje: "Login satisfactorio.",
                    rows,
                });
            }else{
                res.json({
                    mensaje: "Login Error.",
                    code: 400,
                });
            }
        });
    });
});

// obtener usuario por id
router.get('/usuario/:id', function (req, res) {
    const  id  = req.params.id;
    console.log(id);
    req.getConnection((err, conn) => {
        conn.query("SELECT * FROM usuario WHERE id = ?", [id], (err, rows) => {
            if(err){
                console.log(err);
                res.json(err);
            }
            addLog(" >> Usuario existe.");
            res.json({
                mensaje: "Usuario existe.",
                rows: rows[0]
            });
        });
    });
});

//actualizar un usuario
router.put('/usuario/:id', function (req, res) {
    const id = req.params.id;
    const user = req.body;
    req.getConnection((err, conn) => {
        conn.query('UPDATE usuario set ? where id = ?', [user, id], (err, rows) => {
            addLog(" >> Usuario actualizado.");
            res.json({
                mensaje: "Usuario actualizado."
            });
        });
    });
});

//eliminar un usuario
router.delete('/usuario/:id', function (req, res) {
    const id = req.params.id;
    req.getConnection((err, conn) => {
        conn.query('DELETE from usuario where id = ?', [id], (err, rows) => {
            addLog(" >> Usuario eliminado.");
            res.json({
                mensaje: "Usuario eliminado."
            });
        });
    });
});
*/
module.exports = router;
