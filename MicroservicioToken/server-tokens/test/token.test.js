const assert = require('assert');
const Token=require('../server/token');

let microservicio=new Token.Token();


describe('Test Microservicio Token', () => {
    it('Is valid Json response from Token microservice {status:200} should return true', () => {
            const respuesta={status:200,titulo:'Elecciones Presidente'};
            const res=microservicio.isValidJsonResult(respuesta);
            assert.ok(res);
       });
    it('Validate negative id -1 should return false', () => {
            const res=microservicio.isIdValido(-1);
            assert.equal(res,false);
       });
    it('Validate positive id 4 should return true', ()=>{
            const res=microservicio.isIdValido(4);
            assert.ok(res);
        });
   });
