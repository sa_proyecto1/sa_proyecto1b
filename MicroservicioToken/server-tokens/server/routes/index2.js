'use strict';

var express = require('express');
const path = require('path');
const fs = require('fs');
// https://github.com/auth0/node-jsonwebtoken
const jwt = require('jsonwebtoken');
var app = express();


//********* LOG ***********************/
var logger = fs.createWriteStream('./log.txt', {
    flags: 'a' // 'a' means appending (old data will be preserved)
})

var today = new Date();
var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();


function addLog(newlog){
    logger.write(date+" "+time+" - "+newlog+"\n");
}


//********* BASE DE DATOS ***********************/
/*
const mysql = require('mysql');
const con = mysql.createConnection({
    host: 'db-mysql-token',
    port: 3306,
    user: "root",
    password: "1234",
    database: "prsa"
});
  
con.connect(function(err) {
    if (err) console.log(err);
    console.log("Connected!");
});
*/
//***********************************************/


var privateKey = fs.readFileSync('./private.key','utf8');
//var publicKey = fs.readFileSync('./public.key','utf8');

var payload={};
console.log("Payload: " + JSON.stringify(payload));

/*
    Sign
*/
// Expiration timespan: https://github.com/auth0/node-jsonwebtoken#token-expiration-exp-claim
var exp = "60s";

// JWT Token Options, see: https://tools.ietf.org/html/rfc7519#section-4.1 for the meaning of these
var signOptions = {
    expiresIn: exp,//expiracion en
    algorithm: "RS256" //algoritmo
};
console.log("Options: " + JSON.stringify(signOptions));
console.log("\n");


/** RUTA OBTENCION DE TOKEN */
app.post('/token', function (req, res) {
    var userpass = Buffer.from((req.headers.authorization || '').split(' ')[1] || '', 'base64').toString();

    var arrayDeCadenas = userpass.split(':');
    console.log(arrayDeCadenas[0]);
    console.log(arrayDeCadenas[1]);
    addLog('>> Usuario que esta pidiendo un token: '+arrayDeCadenas);

    var result=undefined;
    
    if (arrayDeCadenas[1] == "secret") {
        //console.log("entro al if", arrayDeCadenas)
            payload.valores = arrayDeCadenas;
            
            jwt.sign(payload, privateKey, signOptions,(err,token)=>{
                addLog('>> Token que se enviara: '+token);
                //console.log('>> Token que se enviara: '+token);
                addLog('>>');
                    res.json({
                        jwt: token
                        
                    })

                    return;
                });       

        
        
    }else{
        res.writeHead(401, { 'Basic-Authenticate': 'Basic realm="nope"' });
        res.end('HTTP Error 401 Unauthorized: Access is denied');
        addLog('>> HTTP Error 401 Unauthorized: Access is denied');
        return;

    }



});


app.listen(3000, function () {
    console.log('Servidor de Tokens, listening on port 3000!');
    addLog('>> Servidor de Tokens, listening on port 3000 in docker and 88 in vm!');
});

  
