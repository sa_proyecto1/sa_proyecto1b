import { AxiosError, AxiosResponse } from "axios";
var axios = require('axios');
const express = require('express');
const db = require('../db/index');
const router = express();
const mysql = require('mysql');
const morgan = require('morgan');
const myConnection = require('express-myconnection');
const bodyParser = require('body-parser');
var axios = require('axios');
var jwt=require('jsonwebtoken');
const fs = require('fs');
const path = require('path');


let respuesta = {
    error: false,
    codigo: 200
   };


//TODO ESTO ES PARA DESENCRIPTAR TOKENS QUE SOLICITAN CONSUMIR TUS SERVICIOS DE TU MICROSERVICIO
var publicKey = fs.readFileSync('./server/public.key','utf8');
var verifyOptions = {

    algorithms: ["RS256"],
    maxAge: "60s"    
};


// middlewares
router.use(morgan('dev'));
router.use(myConnection(mysql, {
    host: 'db-mysql-registro',
    port: '3306',
    user: 'admin',
    password: 'root',
    database: 'prsa'
  }, 'single'));

router.use(express.urlencoded({extended: true}));

router.use(bodyParser.urlencoded({
    extended: true
}));

router.use(bodyParser.json());

var logger = fs.createWriteStream('./server/log.txt', {
    flags: 'a' // 'a' means appending (old data will be preserved)
})

function addLog(newlog){
    logger.write(newlog+"\n");
}

var http = require('http');


router.get('/todos', async (req, res, next) => {
    try{
        let results = await db.all();
        res.json(results);
        
    } catch(e) {
        console.log(e);
        res.sendStatus(500);
    }
});

router.post('/autenticacion', async (req, res, next) => {
    const username = 'grupo09';
    const password = 'gR4p0_09';
    const email = req.body.email;
    const pass = req.body.pass;    
    const token = Buffer.from(`${username}:${password}`, 'utf8').toString('base64')

    /*axios.post('http://35.232.54.106/token')
    .then(function(response:AxiosResponse){
        res.send(res.json(response.data.token));
    }).catch(function(e:AxiosError){
        res.send(e.message);
    });*/
    if(email == "grupo09" && pass == "gR4p0_09"){
        axios.post('http://34.70.175.232:88/api/token',{
        },{
            headers:{
                'Authorization': `Basic `+ token 
            }
        })
        .then(function(response:AxiosResponse){
            res.send(response.data);
            //res.send(email + " " + pass);
        }).catch(function(e:AxiosError){
            res.send(e.message);
        });
    }else{
        res.send("Error de usuario y Contraseña")
    }
});

router.post('/registarUser', async (req, res, next) => {
    functionLista(req, res, async function() {

    }.bind(this));
});


function functionLista(req, res, callback){
    listarUsuario(req, res, function(){
        return callback();
    }.bind(this));
}


function listarUsuario(req, res, callback){
    console.log("Verificar auth...")
    llamarServicioAuth(function(bearerToken){
        jwt.verify(bearerToken,publicKey,verifyOptions,(err)=>{
            if(err){
                console.log("error auth...");
                return callback();
            }else{
                //Logica para tirar dados......!
                console.log("llamando servicio listar...");

                llamarServicioListar(req, res, bearerToken, function(){

                    return callback();
                }.bind(this));
            }
        });
    });
}

function llamarServicioAuth(callback){
    var objAuth = {'Authorization': 'Basic Z3J1cG8wOTpnUjRwMF8wOQ=='};
    var optionsAuth = {
        host : '34.70.175.232',
        path : '/api/token',
        method : 'POST',
        port: 89,
        headers: objAuth
    };
    var bearerToken = "";
    console.log("->>>>>>  Consumiendo servicio de RENAP -> POST -> 34.70.175.232/api/token")
    var reqAuth = http.request(optionsAuth, function(res) {
        console.log("statusCode: ", res.statusCode);
        res.on('data', function(d) {
            //console.info('POST result:'+d+'\n');
            //process.stdout.write(d+'\n');
            var data = JSON.parse(d);

            bearerToken = data.token;
            console.log("token: "+bearerToken);
            addLog("autenticación de token"+bearerToken+"\n");

            return callback(bearerToken);
        });
    }.bind(this));
    reqAuth.end();
}


function llamarServicioListar(req, res, bearerToken, callback) {
    var objAuth = {'Authorization': 'Bearer '+bearerToken};
    
    console.log(bearerToken);
    console.log(req.body.cui);
    var j = axios.get(`http://34.70.175.232:87/api/renap/${req.body.cui}`, {
        headers:{
            'Authorization': `Bearer ${bearerToken}` 
        }
    })
    .then(function(response:AxiosResponse){
        console.log("->>>>>>  Consumiendo servicio de listarUsuarios -> POST -> http://34.70.175.232:87/api/renap/:id")

        if(response.data.cui == req.body.cui && response.data.nombres == req.body.nombre && response.data.apellidos == req.body.apellido) {
            console.log("Usuario encontrado");
            console.log(response);
            addLog("Usuario encontrado en renap\n");
            var user = response.data;
            db.insertarUsuario(user.cui,user.nombres,user.apellidos,user.fecha_nacimiento, user.lugar_municipio, user.lugar_departamento, user.lugar_pais, user.nacionalidad, user.sexo, user.estado_civil, user.servicio_militar, user.privado_libertad, user.foto, user.padron, req.body.email, req.body.password, user.ip);
            res.send(response.data);
            return callback();
        }

        addLog("Usuario no encontrado en renap\n");
        res.send("Usuario no encontrado en renap\n");
        return callback("Usuario no existe");
        
    }).catch(function(e:AxiosError){
        console.log("Error bro :(");
        console.log(e);
        res.send(e.message);
    });
}

function pedirToken(){
    const username = '';
    const password = 'secret';
    const token = Buffer.from(`${username}:${password}`, 'utf8').toString('base64');
        axios.post('http://34.70.175.232:88/api/token',{
        },{
            headers:{
                'Authorization': `Basic `+ token 
            }
        })
        .then(function(response:AxiosResponse){
            //res.send(email + " " + pass);
            return response.data;
        }).catch(function(e:AxiosError){
            return e.message;
        });
};

//login
router.post('/login', function (req, res) {
    const user = req.body;
    console.log("esto trae el body"+req.body);
    req.getConnection((err, conn) => {
        conn.query("SELECT * FROM Usuario WHERE email = ?", [user.email], (err, rows) => { 
            if(err){
                console.log(err);
                res.json(err);
            }
            if(user.password == rows[0].password){
                addLog(" >> Login satisfactorio para usuario ");
                res.json({
                    mensaje: "Login satisfactorio.",
                    rows,
                });
            }else{
                res.json({
                    mensaje: "Login Error.",
                    code: 400,
                });
            }
        });
    });
});
/*
router.post('/todos',   async (req, res, next) => {
    var jsonsalida : any = { 406: "error con comunicación a base de datos" };
    try{
        let respuesta = await db.all();
        jsonsalida = {"estado":201, "respuesta": respuesta};
        res.json(jsonsalida);
        addLog("login exitoso"+"\n");
    } catch(e) {
        console.log(e);
        res.sendStatus(500);
    }
});


router.post('/login3',   async (req, res, next) => {
    var jsonsalida : any = { 406: "error con comunicación a base de datos" };
    try{
        let respuesta = await db.oneAll(req.body.cui);
        jsonsalida = {"estado":201, "respuesta": respuesta};
        res.json(jsonsalida);
        addLog("login exitoso"+"\n");
    } catch(e) {
        console.log(e);
        res.sendStatus(500);
    }
});
*/
module.exports = router