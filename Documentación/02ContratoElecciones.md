![Help Builder Web Site](./Logo.png)
# Proyecto_SA_Grupo2
### Modulo "Registro de usuarios en iVoting"

**ID: SA-002**
<br>
**Nombre: Creación de elecciones**


<table>
<thead>
	<tr>
		<th>Historia de usuario</th>
	</tr>
</thead>
<tbody>
	<tr>
		<td>Como administrador del sistema
Quiero un servicio REST
Para poder crear elecciones crear elecciones
</td>
	</tr>
</tbody>
</table>




<table>
<thead>
	<tr>
		<th>Prioridad</th>
		<th>Estimado</th>
		<th>Modulo</th>
	</tr>
</thead>
<tbody>
	<tr>
		<td>Alta</td>
		<td>10 puntos</td>
		<td>Elecciones</td>
	</tr>
</tbody>
</table>



**Criterios de Aceptación**
<br>

El servicio debe tener la siguiente configuración:

<br>
<table>
<tbody>
	<tr>
	<td>Ruta</td>
	<td>/api/elecciones</td>
	</tr>
	<tr>
	<td>Método</td>
	<td>POST</td>
	</tr>
	<tr>
	<td>Formato de entrada</td>
	<td>JSON</td>
	</tr>
</tbody>
</table>

<br>

**Header**
<table>
<thead>
	<tr>
		<th>Atributo</th>
		<th>Tipo</th>
		<th>Descripción</th>
	</tr>
</thead>
<tbody>
	<tr>
		<td>Content-type</td>
		<td>header</td>
		<td>application/json</td>
	</tr>
	<tr>
		<td>Authentication</td>
		<td>header</td>
		<td>token <TOKEN></td>
	</tr>
</tbody>
</table>

<br>


**Parametros de Entrada**
<table>
<thead>
	<tr>
		<th>Atributo</th>
		<th>Tipo</th>
		<th>Descripción</th>
	</tr>
</thead>
<tbody>
	<tr>
		<td>título</td> 
		<td>Cadena</td>
		<td>
Título de la elección.
</td>
	</tr>
	<tr>
		<td>descripción</td> 
		<td>Cadena</td>
		<td>
Descripción de la elección.
</td>
	</tr>
	<tr>
		<td>fecha_inicio</td> 
		<td>Date</td>
		<td>Fecha de inicio de la elección.</td>
	</tr>
	<tr>
		<td>fecha_fin</td> 
		<td>Date</td>
		<td>Fecha de finalización de la elección.</td>
	</tr>
	<tr>
		<td>opciones</td> 
		<td>Arreglo</td>
		<td>Arreglo de todos los candidatos en la elección.
		</td>
	</tr>
</tbody>
</table>
<br>



<table>
<tbody>
	<tr>
	<td>Formato de Salida</td>
	<td>JSON</td>
	</tr>
	<tr>
	<td>Código de respuesta exitosa</td>
	<td>HTTP 200</td>
	</tr>
</tbody>
</table>

**Parametros de Salida Exitosa**
<table>
<thead>
	<tr>
		<th>Atributo</th>
		<th>Tipo</th>
		<th>Descripción</th>
	</tr>
</thead>
<tbody>
	<tr>
		<td>id</td> 
		<td>Entero</td>
		<td>
Código de la elección en el sistema.
</td>
	</tr>
		<tr>
		<td>título</td> 
		<td>Cadena</td>
		<td>Título de la elección.</td>
	</tr>
	<tr>
		<td>descripción</td> 
		<td>Cadena</td>
		<td>Correo electrónico del usuario.
		</td>
	</tr>
	<tr>
		<td>fecha_inicio</td> 
		<td>Date</td>
		<td>Fecha de inicio de la elección.
		</td>
	</tr>
	<tr>
		<td>fecha_fin</td> 
		<td>Date</td>
		<td>Fecha de finalización de la elección.
		</td>
	</tr>
	<tr>
		<td>opciones</td> 
		<td>Arreglo</td>
		<td>Arreglo de todos los candidatos en la elección.</td>
	</tr>
</tbody>
</table>
<br>

**Código de respuesta fallida**
<table>
<thead>
	<tr>
		<th>Código</th>
		<th>Descripción</th>
	</tr>
</thead>
<tbody>
	<tr>
		<td>500</td> 
		<td>Error al registrar la elección.</td>
	</tr>

</tbody>
</table>
<br>

**Parámetros de salida fallida:**
<table>
<thead>
	<tr>
		<th>Atributo</th>
		<th>Tipo</th>
		<th>Descripción</th>
	</tr>
</thead>
<tbody>
	<tr>
		<td>status</td> 
		<td>Cadena</td> 
		<td>
Indica el tipo de error ocurrido.
</td> 
	</tr>
	<tr>
		<td>mensaje</td> 
		<td>Cadena</td> 
		<td>Muestra un detalle del error ocurrido.</td> 
	</tr>
</tbody>
</table>
<br>


**Ejemplo de parámetros de entrada:**
>{
	"titulo": “Elecciones presidenciales”,
	"descripcion": “Elecciones para presidente de la República de Guatemala”,
    "fecha_inicio": “DD/MM/YYYY”,
	"fecha_fin": “DD/MM/YYYY”,
    "opciones": [
            "PAN":{
			"postulante": { “presidente”:“Juan Paco”,”vicepresidente”:”pedro delamar” },
			"votos_recibidos":42,
			"ubicacion":[
			{
				"pais": "",
				"municipio": "Mixco",
				"departamento": "Guatemala",
				"cantidad_votos": 0
			},
			{
				"pais": "",
				"municipio": "Antigua Guatemala",
				"departamento": "Sacatepequez",
				"cantidad_votos": 0
			}
			]
		},
		"UNE":{
			"postulante": { “presidente”: "Pedro", vicepresidente: “Manuel Carias”},
			"votos_recibidos": 0,
			"ubicacion":[
			{
				"pais": "",
				"municipio": "Mixco",
				"departamento": "Guatemala",
				"cantidad_votos": 0
			},
			{
				"pais": "",
				"municipio": "Antigua Guatemala",
				"departamento": "Sacatepequez",
				"cantidad_votos": 0
			},
			{
				"pais": "USA",
				"municipio": "",
				"departamento": "",
				"cantidad_votos": 0
			}]
>}

**Ejemplo de parámetros de salida exitosa:**
>{
    “id”: “_id000001”,
	"titulo": “Elecciones presidenciales”,
	"descripcion": “Elecciones para presidente de la República de Guatemala”,
    "fecha_inicio": “DD/MM/YYYY”,
	"fecha_fin": “DD/MM/YYYY”,
>}

**Ejemplo de parámetros de salida fallida:**
>{
	"status": 400,
	"mensaje" “”:    
>}
