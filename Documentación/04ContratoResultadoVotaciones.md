![Help Builder Web Site](./Logo.png)
# Proyecto_SA_Grupo2
### Modulo "Registro de usuarios en iVoting"

**ID: SA-004**
<br>
**Nombre: Resultados de votaciones**


<table>
<thead>
	<tr>
		<th>Historia de usuario</th>
	</tr>
</thead>
<tbody>
	<tr>
		<td>Como administrador del sistema
Quiero un servicio REST
Para poder crear elecciones crear elecciones
</td>
	</tr>
</tbody>
</table>




<table>
<thead>
	<tr>
		<th>Prioridad</th>
		<th>Estimado</th>
		<th>Modulo</th>
	</tr>
</thead>
<tbody>
	<tr>
		<td>Alta</td>
		<td>10 puntos</td>
		<td>Elecciones</td>
	</tr>
</tbody>
</table>

<br>

**Criterios de Aceptación**
<br>

El sistema debe validar que la elección esté activa y garantizar el voto secreto para los usuarios.

El servicio debe tener la siguiente configuración:


<br>
<table>
<tbody>
	<tr>
	<td>Ruta</td>
	<td>/api/resultado/:ideleccion</td>
	</tr>
	<tr>
	<td>Método</td>
	<td>GET</td>
	</tr>
	<tr>
	<td>Formato de entrada</td>
	<td>explícito en la ruta.</td>
	</tr>
</tbody>
</table>


<br>

**Header**
<table>
<thead>
	<tr>
		<th>Atributo</th>
		<th>Tipo</th>
		<th>Descripción</th>
	</tr>
</thead>
<tbody>
	<tr>
		<td>Content-type</td>
		<td>header</td>
		<td>application/json</td>
	</tr>
	<tr>
		<td>Authentication</td>
		<td>header</td>
		<td>token <TOKEN></td>
	</tr>
</tbody>
</table>

<br>


**Parametros de Entrada**
<table>
<thead>
	<tr>
		<th>Atributo</th>
		<th>Tipo</th>
		<th>Descripción</th>
	</tr>
</thead>
<tbody>
	<tr>
		<td>id_elección</td> 
		<td>Cadena</td>
		<td>Identificador de la elección donde va a votar.
</td>
	</tr>
</tbody>
</table>
<br>

<table>
<tbody>
	<tr>
	<td>Formato de Salida</td>
	<td>JSON</td>
	</tr>
	<tr>
	<td>Código de respuesta exitosa</td>
	<td>HTTP 200</td>
	</tr>
</tbody>
</table>

<br>

**Parametros de Salida Exitosa**
<table>
<thead>
	<tr>
		<th>Atributo</th>
		<th>Tipo</th>
		<th>Descripción</th>
	</tr>
</thead>
<tbody>
	<tr>
		<td>Token</td> 
		<td>Cadena</td>
		<td>
Token de autenticación
</td>
	</tr>
	<tr>
		<td>titulo</td> 
		<td>Cadena</td>
		<td>
Título de la elección.
</td>
	</tr>
	<tr>
		<td>fecha</td> 
		<td>Date</td>
		<td>
Fecha en que se realizó.
</td>
	</tr>
	<tr>
		<td>reg_votantes</td> 
		<td>Entero</td>
		<td>Número de votantes registrados al momento de la elección.
		</td>
	</tr>
	<tr>
		<td>num_votantes</td> 
		<td>Entero</td>
		<td>Número de votantes que votaron durante la elección.</td>
	</tr>
	<tr>
		<td>votos</td> 
		<td>Arreglo</td>
		<td>Arreglo de votos de los usuarios, incluyendo votos nulos y localización.
		</td>
	</tr>
</tbody>
</table>
<br>

**Código de respuesta fallida**
<table>
<thead>
	<tr>
		<th>Código</th>
		<th>Descripción</th>
	</tr>
</thead>
<tbody>
	<tr>
		<td>500</td> 
		<td>Error al registrar la elección.</td>
	</tr>

</tbody>
</table>
<br>

**Parámetros de salida fallida:**
<table>
<thead>
	<tr>
		<th>Atributo</th>
		<th>Tipo</th>
		<th>Descripción</th>
	</tr>
</thead>
<tbody>
	<tr>
		<td>status</td> 
		<td>Cadena</td> 
		<td>
Indica el tipo de error ocurrido.
</td> 
	</tr>
	<tr>
		<td>mensaje</td> 
		<td>Cadena</td> 
		<td>Muestra un detalle del error ocurrido.</td> 
	</tr>
</tbody>
</table>
<br>

**Ejemplo de parámetros de entrada:**
>{
	"id_eleccion": “_id00001”
>}

**Ejemplo de parámetros de salida exitosa:**
>{

	“status”:200,
    "titulo":”TituloEleccion”,
	"fecha_inicio":”DD/MM/YYYY”,
	"fecha_fin":”DD/MM/YYYY”,
	"votantes_registrados":127,
	"votos_totales":#####,
	"cargo":#,
	"datos_votacion":[
		"nulos":{
			"postulante": "nulo",
			"votos_recibidos":42,
			"ubicacion":[
			{
				"pais": "",
				"municipio": "Mixco",
				"departamento": "Guatemala",
				"cantidad_votos": 40
			},
			{
				"pais": "",
				"municipio": "Antigua Guatemala",
				"departamento": "Sacatepequez",
				"cantidad_votos":2
			}
			]
		},
		"PAN":{
			"postulante": { “presidente”:“juan paco”,”vicepresidente”:”pedro delamar” },
			"votos_recibidos":42,
			"ubicacion":[
			{
				"pais": "",
				"municipio": "Mixco",
				"departamento": "Guatemala",
				"cantidad_votos": 40
			},
			{
				"pais": "",
				"municipio": "Antigua Guatemala",
				"departamento": "Sacatepequez",
				"cantidad_votos":2
			}
			]
		},
		"UNE":{
			"postulante": { “presidente”: "Pedro", vicepresidente: “Manuel Carias”},
			"votos_recibidos": 45,
			"ubicacion":[
			{
				"pais": "",
				"municipio": "Mixco",
				"departamento": "Guatemala",
				"cantidad_votos": 40
			},
			{
				"pais": "",
				"municipio": "Antigua Guatemala",
				"departamento": "Sacatepequez",
				"cantidad_votos":2
			},
			{
				"pais": "USA",
				"municipio": "",
				"departamento": "",
				"cantidad_votos":3
			}
			]
		}
	]	
>}



**Ejemplo de parámetros de salida fallida:**
>{
	"status": 400,
	"mensaje": ""
>}
