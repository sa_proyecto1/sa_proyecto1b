import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProductComponent } from './product/product.component';
import { LoginComponent } from './login/login.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule} from '@angular/material/button';
import { MatFormFieldModule} from '@angular/material/form-field';
import { MatInputModule} from '@angular/material/input';
import { EleccionComponent } from './eleccion/eleccion.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { RegistroComponent } from './registro/registro.component';
import { InicioVotanteComponent } from './inicio-votante/inicio-votante.component';
import { MenuVComponent } from './menu-v/menu-v.component';
import { MenuComponent } from './menu/menu.component';
import { VotacionComponent } from './votacion/votacion.component';
import { ResVotacionComponent } from './res-votacion/res-votacion.component';   

import { ChartsModule } from 'ng2-charts';
import {AgmCoreModule} from '@agm/core';

import { DatePipe } from '@angular/common';
//import { ChartsModule } from 'ng2-charts';
import { ApiService } from './services/api.service';
import { ConectionService } from './services/conection.service';
import { SalirComponent } from './salir/salir.component';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { InicioAdminComponent } from './inicio-admin/inicio-admin.component';
import { MenuAdmComponent } from './menu-adm/menu-adm.component';
import { MostrareleccionComponent } from './mostrareleccion/mostrareleccion.component';


@NgModule({
  declarations: [
    AppComponent,
    ProductComponent,
    LoginComponent,
    EleccionComponent,
    DashboardComponent,
    RegistroComponent,
    InicioVotanteComponent,
    MenuVComponent,
    MenuComponent,
    VotacionComponent,
    ResVotacionComponent,
    SalirComponent,
    InicioAdminComponent,
    MenuAdmComponent,
    MostrareleccionComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatCardModule,
    //ChartsModule,
    MatButtonModule,
    ChartsModule,
    AgmCoreModule.forRoot({
      apiKey:'AIzaSyA0QxSDQZcqCuURTT2r5pBC_Vg5KK1-TLg'
    }),
    MatFormFieldModule,
    MatInputModule
  ],
  providers: [ConectionService, DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
