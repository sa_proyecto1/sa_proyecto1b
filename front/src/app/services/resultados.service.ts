import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})


export class ResultadosService {

  endpoint = 'http://localhost:3005/api/';
  microServicioEleccion='http://localhost:3005/api/';
  
  

  constructor(private http: HttpClient) { }


  getResultados(ideleccion: Number,urlResultados:string) {
    let httpOptions = {
      headers : new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Authorization': 'token ' + sessionStorage.getItem('token')
      })
    };
    return this.http.get<any>(urlResultados+ideleccion, httpOptions);
  }

  getElecciones(urlElecciones:string) {
    let httpOptions = {
      headers : new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Authorization': 'token ' + sessionStorage.getItem('token')
      })
    };
    return this.http.get<any>(urlElecciones, httpOptions);
  }

}
