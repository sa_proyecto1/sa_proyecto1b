import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { RutasService } from './rutas.service';

var httpAddress = 'http://34.70.175.232:81/';


@Injectable({
  providedIn: 'root'
})


export class ConectionService {

  constructor(private httpClient: HttpClient, private rutas: RutasService) { }

  PostRequest(serverAddress: string, info: object) {   
    console.log(info);
    const httpOptions = {
      headers : new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*'
        //'Authorization': 'Basic ' + token
      })
    };
    return this.httpClient.post<any>(httpAddress + serverAddress, info, httpOptions);
  }


  PostRequest2(serverAddress: string, info: object) {   
    const httpOptions = {
      headers : new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*'
      })
    };
    return this.httpClient.post<any>(httpAddress+serverAddress, info, httpOptions);
  }

  PostRequest3(serverAddress: string, info: object) {   
    const httpOptions = {
      headers : new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*'
      })
    };
    return this.httpClient.post<any>(serverAddress, info, httpOptions);
  }


  PostRequest4(serverAddress: string, info: object, token: string){   
    console.log(info);
    const httpOptions = {
      headers : new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Authorization': 'Basic ' + token
      })
    };
    return this.httpClient.post<any>(serverAddress, info, httpOptions);
  }


  GetRequest(serverAddress: string, token: string) {
    const httpOptions = {
      headers : new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Authorization': 'Basic ' + token
      })
    };
    return this.httpClient.get<any>(serverAddress, httpOptions);
  }
}
