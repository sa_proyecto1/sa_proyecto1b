import { Component, OnInit } from '@angular/core';
import { ConectionService } from '../services/conection.service';
import { Router } from '@angular/router';
import {FormControl} from '@angular/forms';
import { IpServiceService } from '../ip-service.service';
import { DatePipe } from '@angular/common'
import { withLatestFrom } from 'rxjs/internal/operators';
@Component({
  selector: 'app-votacion',
  templateUrl: './votacion.component.html',
  styleUrls: ['./votacion.component.css']
})


export class VotacionComponent implements OnInit {
  todayNumber: number = Date.now();
  todayDate : Date = new Date();
  date: any;
  eleccion: string = '';
  partido: string = '';
  pin: string = '';
  alerta: string = '';
  usuario: string = '';
  userid : any;
  fecha : any;
  hora:any;
  toppings = new FormControl();
  contact = {
    firstName: "CFR",
    comment: "No comment",
    subscribe: true,
    contactMethod: 2 // this id you'll send and get from backend
  }
  
  Elecciones = [
    { id: 1, label: "Email" },
    { id: 2, label: "Phone" }
  ]

  Partidos = [
    { id: 1, label: "Email" },
    { id: 2, label: "Phone" }
  ]

  pinc(): void{

  }

  constructor(public datepipe: DatePipe, private ip:IpServiceService, private con: ConectionService, private router: Router) { }
  
  votar(): void{

  }
    
  ngOnInit(): void {
    this.userid = sessionStorage.getItem('userid');
    if (!this.userid){
      //this.router.navigate(['login']);
    }
    this.llenarEleccion();
    this.llenarPartido();
  }

  createPartidoObject(eleccion: string) {
    return { eleccion: eleccion};
  }

  createEleccionObject(fecha: string, hora: string) {
    return { fecha: fecha, hora: hora };
  }
  
  horas(){
    this.date=new Date();
    let latest_date =this.datepipe.transform(this.date, 'h:mm:ss');
    return latest_date;
  }

  fechas(){
    this.date=new Date();
    let latest_date =this.datepipe.transform(this.date, 'yyyy-MM-dd');
    return latest_date;
   }

  llenarEleccion(){
    this.fecha = this.fechas()?.toString();
    this.hora = this.horas()?.toString();
    console.log(this.horas());
    console.log(this.fechas());
    let eleccion = this.createEleccionObject(this.fecha, this.fecha);
    this.con.PostRequest2('login', eleccion).toPromise()
      .then((res) => {
        console.log("ya funciono");  
        console.log(res.res);
        res.res = 
        { id: 1, label: "Email" },
        { id: 2, label: "Phone" }
        ;
        this.Elecciones = [res.res];
      })
      .catch((err)=>{
        setTimeout(() => this.alerta = err.error.mensaje, 0);
      });
  }

  llenarPartido(){
    let eleccion = this.createPartidoObject(this.eleccion);
    this.con.PostRequest2('login', eleccion).toPromise()
      .then((res) => {
        console.log("ya funciono");  
        console.log(res.res);
        res.res = 
        { id: 1, label: "Email" },
        { id: 2, label: "Phone" }
        ;
        this.Partidos = [res.res];
      })
      .catch((err)=>{
        setTimeout(() => this.alerta = err.error.mensaje, 0);
      });
  }

  mostrar(): void {
    
  }
}

