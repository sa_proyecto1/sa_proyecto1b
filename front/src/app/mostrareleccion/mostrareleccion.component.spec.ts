import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MostrareleccionComponent } from './mostrareleccion.component';

describe('MostrareleccionComponent', () => {
  let component: MostrareleccionComponent;
  let fixture: ComponentFixture<MostrareleccionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MostrareleccionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MostrareleccionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
