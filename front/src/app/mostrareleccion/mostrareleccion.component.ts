import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { storage_v1 } from 'googleapis';
import { ConectionService } from '../services/conection.service';
import { RutasService } from '../services/rutas.service';

@Component({
  selector: 'app-mostrareleccion',
  templateUrl: './mostrareleccion.component.html',
  styleUrls: ['./mostrareleccion.component.css']
})
export class MostrareleccionComponent implements OnInit {

  // Exporto los datos del archivo JSON a la  vista 
  resultado: any ;
  alerta: string = "";
  correo:any = "";
  password:any = "";
  token: any = "";

  constructor(private con: ConectionService, private rutas:RutasService, private router: Router) { }

  ngOnInit(): void {
    this.correo = sessionStorage.getItem('usuario');
    this.password = sessionStorage.getItem('contra');
    this.token = sessionStorage.getItem('token');
    this.obtener();
  }

  createLoginGObject(email: string, pass: string) {
    return { email: email, pass: pass};
  }

  obtener() {
    console.log("entro");
    console.log(this.correo);
    console.log(this.password);
    console.log(this.token);
    let login:any;
    login = this.createLoginGObject(this.correo, this.password);
    let rep= this.rutas.devolverEleccion(this.correo,this.password);
    console.log(rep);
    this.con.GetRequest(rep.url + rep.api, this.token).toPromise()
    .then((res) => {
      console.log(res);
      this.resultado = res;
    })
    .catch((err)=>{
      setTimeout(() => this.alerta = err.error.mensaje, 0);
    });
  }
}