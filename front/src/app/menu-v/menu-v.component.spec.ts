import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuVComponent } from './menu-v.component';

describe('MenuVComponent', () => {
  let component: MenuVComponent;
  let fixture: ComponentFixture<MenuVComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MenuVComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuVComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
