import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { IpServiceService } from '../ip-service.service';
import { ApiService } from '../services/api.service';
import { ConectionService } from '../services/conection.service';
@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})

export class RegistroComponent implements OnInit {

  constructor(private ip:IpServiceService, private con: ConectionService,private api: ApiService, private router: Router) {
  }

  cui: string = '';
  nombre: string = '';
  apellido: string = '';
  email: string = '';
  password: string = '';
  alerta: string = '';
  title = 'DemoApp';  
  ipAddress:string | undefined;  
  jwt : string = '';

  ngOnInit()  
  {  
    this.getIP();  
    this.crearToken();
  }  
  
  crearToken(){
    let valor = this.createObject();
    this.api.PostRequest("api/token",valor).toPromise()
    .then((res)=>{
      this.jwt = res['jwt'];
      //console.log(this.jwt);
    })
    .catch((err)=>{
      setTimeout(() => this.alerta = err.error.mensaje, 0);
    });
  }


  getIP()  
  {  
    this.ip.getIPAddress().subscribe((res:any)=>{  
      this.ipAddress=res.ip;  
    });  
  }  

  createRegistroObject(cui: string, nombre:string, apellido: string, email: string, password:string, ip: any) {
    return { cui: cui, nombre: nombre, apellido: apellido, email: email, password:password, ip: ip };
  }

  createObject() {
    return {};
  }

  onSubmit() {
  }

/*
  {
    "cui": "2222111111120",
    "nombre": "Sergio da Roberto",
    "apellido": "Perez Lopez",
    "email": "sergio@email.com",
    "password": "pass123"
}
fecha_nacimiento, this.lugar_municipio, this.lugar_departamento, 
        this.lugar_pais, this.nacionalidad, this.sexo, this.estado_civil, this.servicio_militar, 
        this.privado_libertad, this.foto, this.padron, 
*/

  registro() {
    let registros = this.createRegistroObject(this.cui, this.nombre, this.apellido, this.email, this.password, this.getIP());
        //console.log(this.jwt);
        this.con.PostRequest('api/registarUser', registros).toPromise()
        .then((res) => {
          console.log("ya funciono"); 
          if(res['cui']){
            console.log("ya funciono");   
            console.log(res);
            sessionStorage.setItem('userid', res.userid);
            this.router.navigate(['inicioVotante']);
          }else{
            this.alerta = "Error al momento de autenticación F"
            console.log(this.alerta);
          }
        })
        .catch((err)=>{
          setTimeout(() => this.alerta = err.error.mensaje, 0);
        });

    //this.router.navigate(['inicioVotante']);
  }

  enviarCorreo(){
    /*
    this.http.sendEmail("http://localhost:3000/sendmail", user).subscribe(
      data => {
        let res:any = data; 
        console.log(
          `👏 > 👏 > 👏 > 👏 ${user.name} is successfully register and mail has been sent and the message id is ${res.messageId}`
        );
      },
      err => {
        console.log(err);
        this.loading = false;
        this.buttionText = "Submit";
      },() => {
        this.loading = false;
        this.buttionText = "Submit";
      }
    );
    */
  }
}


