import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductComponent } from './product/product.component';
import { LoginComponent } from './login/login.component';
import { EleccionComponent } from './eleccion/eleccion.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { RegistroComponent } from './registro/registro.component';
import { InicioVotanteComponent } from './inicio-votante/inicio-votante.component';
import { VotacionComponent } from './votacion/votacion.component';
import { ResVotacionComponent } from './res-votacion/res-votacion.component';
import { SalirComponent } from './salir/salir.component';
import { InicioAdminComponent } from './inicio-admin/inicio-admin.component';
import { MostrareleccionComponent } from './mostrareleccion/mostrareleccion.component';
const routes: Routes = [
  {
    path: 'inicio',
    component: ProductComponent,
    data: { title: 'Product List' }
  },
  {
    path: 'login',
    component: LoginComponent,
    data: { title: 'login' }
  },
  {
    path: 'eleccion',
    component: EleccionComponent,
    data: { title: 'elección' }
  },
  {
    path: 'dashboard',
    component: DashboardComponent,
    data: { title: 'dashboard' }
  },
  {
    path: 'registro',
    component: RegistroComponent,
    data: { title: 'registro' }
  },
  {
    path: 'inicioVotante',
    component: InicioVotanteComponent,
    data: { title: 'inicioVotante' }
  },
  {
    path: 'inicio-Admin',
    component: InicioAdminComponent,
    data: { title: 'inicio-Admin' }
  },
  {
    path: 'mostrareleccion',
    component: MostrareleccionComponent,
    data: { title: 'mostrareleccion' }
  },
  {
    path: 'votacion',
    component: VotacionComponent,
    data: { title: 'votacion' }
  },
  {
    path: 'resultados',
    component: ResVotacionComponent,
    data: { title: 'resultados' }
  },
  {
    path: 'salir',
    component: SalirComponent,
    data: { title: 'salir' }
  },
  { 
    path: '',
    redirectTo: '/inicio',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
