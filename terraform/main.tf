provider "google" {
  credentials = file("proyecto-sa-311403-91d9eff167ee.json")
  project = "proyecto-sa-311403"
  region = "us-west1"
}

resource "google_service_account" "default" {
  account_id   = "service-account-id"
  display_name = "Service Account"
}

resource "google_container_cluster" "primary" {
  name     = "my-gke-cluster"
  location = "us-central1"

  # We can't create a cluster with no node pool defined, but we want to only use
  # separately managed node pools. So we create the smallest possible default
  # node pool and immediately delete it.
  remove_default_node_pool = true
  initial_node_count       = 1
}

resource "google_container_node_pool" "primary_preemptible_nodes" {
  name       = "sa-node-pool"
  location   = "us-central1"
  cluster    = google_container_cluster.primary.name
  node_count = 1

  node_config {
    preemptible  = true
    machine_type = "e2-medium"

    # Google recommends custom service accounts that have cloud-platform scope and permissions granted via IAM Roles.
    //service_account = google_service_account.default.email
    service_account = google.credentials.client_email

    oauth_scopes    = [
      "https://www.googleapis.com/auth/cloud-platform"
    ]
    tags = ["allin", "allout"]
  }
}

/*

resource "random_id" "instance_id" {
  byte_length=8
}

resource "google_compute_instance" "default" {
  name="micro-${random_id.instance_id}"
  machine_type = "f1-micro"
  zone = "us-west1-a"

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-9"
    }
  }

  metadata_startup_script = "sudo apt-get update;"

  network_interface {
    network = "default"
    access_config {
      
    }
  }
}
*/